package com.playtech.assignment;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class TransactionProcessor {
    public static void main(String[] args) throws IOException {
        List<User> users = TransactionProcessor.readUsers(Paths.get(args[0]));
        List<Transaction> transactions = TransactionProcessor.readTransactions(Paths.get(args[1]));
        List<BinMapping> binMappings = TransactionProcessor.readBinMappings(Paths.get(args[2]));

        List<Event> events = TransactionProcessor.processTransactions(users, transactions, binMappings);

        TransactionProcessor.writeBalances(Paths.get(args[3]), users);
        TransactionProcessor.writeEvents(Paths.get(args[4]), events);
    }

    private static List<User> readUsers(final Path filePath) {
        List<User> answer = new ArrayList<>();
        try (final FileReader input = new FileReader(filePath.toString());
             final BufferedReader bufferedReader = new BufferedReader(input)) {
            bufferedReader.readLine();
            String line = bufferedReader.readLine();
            while (line != null) {
                String[] data = line.split(",");
                User user = new User(
                        data[0],
                        data[1],
                        Double.parseDouble(data[2]),
                        data[3],
                        Integer.parseInt(data[4]),
                        Double.parseDouble(data[5]),
                        Double.parseDouble(data[6]),
                        Double.parseDouble(data[7]),
                        Double.parseDouble(data[8])
                );
                answer.add(user);
                line = bufferedReader.readLine();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return answer;
    }

    private static List<Transaction> readTransactions(final Path filePath) {
        List<Transaction> answer = new ArrayList<>();
        try (final FileReader input = new FileReader(filePath.toString());
             final BufferedReader bufferedReader = new BufferedReader(input)) {
            bufferedReader.readLine();
            String line = bufferedReader.readLine();
            while (line != null) {
                String[] data = line.split(",");
                Transaction transaction = new Transaction(
                        data[0],
                        data[1],
                        data[2],
                        data[3],
                        data[4],
                        data[5]
                );
                answer.add(transaction);
                line = bufferedReader.readLine();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return answer;
    }

    private static List<BinMapping> readBinMappings(final Path filePath) {
        List<BinMapping> answer = new ArrayList<>();
        try (final FileReader input = new FileReader(filePath.toString());
             final BufferedReader bufferedReader = new BufferedReader(input)) {
            bufferedReader.readLine();
            String line = bufferedReader.readLine();
            while (line != null) {
                String[] data = line.split(",");
                BinMapping binMapping = new BinMapping(
                        data[0],
                        Long.parseLong(data[1]),
                        Long.parseLong(data[2]),
                        data[3],
                        data[4]
                );
                answer.add(binMapping);
                line = bufferedReader.readLine();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return answer;
    }

    private static List<Event> processTransactions(final List<User> users, final List<Transaction> transactions, final List<BinMapping> binMappings) {
        List<Event> answer = new ArrayList<>();
        List<String> usedTransactionIds = new ArrayList<>();
        Set<String> createdAccounts = new HashSet<>();
        Map<String, String> accountUserMap = new HashMap<>();
        Event event;
        User mainUser;
        for (final Transaction transaction : transactions) {
            event = new Event(transaction.transactionId);
            answer.add(event);
            try {
                if (handleNonUniqueTransaction(transaction, usedTransactionIds, event)) continue;
                usedTransactionIds.add(transaction.transactionId);
                mainUser = findValidUser(transaction, users);
                if (handleNonExistingUser(transaction, mainUser, event)) continue;
                if (handleFrozenUser(mainUser, event)) continue;
                if (handleNegativeAmount(transaction, event)) continue;
                if (isTransTypeDeposit(transaction)) {
                    if (handleTransSmallerDepositLimit(transaction, mainUser, event)) continue;
                    if (handleTransBiggerDepositLimit(transaction, mainUser, event)) continue;
                } else if (isTransTypeWithdraw(transaction)) {
                    if (handleTransSmallerWithdrawLimit(transaction, mainUser, event)) continue;
                    if (handleTransBiggerWithdrawLimit(transaction, mainUser, event)) continue;
                    if (handleBalanceTooLow(transaction, mainUser, event)) continue;
                    if (handleNewAccount(transaction, createdAccounts, event)) continue;
                } else {
                    handleBadTransType(event);
                    continue;
                }
                if (isTransMethodTransfer(transaction)) {
                    if (handleInvalidIBAN(transaction, event)) continue;
                    if (handleInvalidCountry(transaction, mainUser, event)) continue;
                } else if (isTransMethodCard(transaction)) {
                    if (handleBadCardType(transaction, binMappings, event)) continue;
                    if (handleInvalidCountry(transaction, mainUser, binMappings, event)) continue;
                } else {
                    handleBadTransMethod(event);
                    continue;
                }
                if (handleAccountUsedByOther(transaction, mainUser, accountUserMap, event)) continue;
                handleSuccess(transaction, mainUser, createdAccounts, accountUserMap, event);
            } catch (Exception e) {
                System.out.println(Arrays.toString(e.getStackTrace()));
                answer.remove(answer.size() - 1);
                usedTransactionIds.remove(usedTransactionIds.size() - 1);
            }
        }
        return answer;
    }

    private static void writeBalances(final Path filePath, final List<User> users) throws IOException {
        try (final FileWriter writer = new FileWriter(filePath.toFile(), false)) {
            writer.append("USER_ID,BALANCE\n");
            for (final var user : users) {
                writer.append(user.userId)
                        .append(",")
                        .append(String.valueOf(user.balance))
                        .append("\n");
            }
        }
    }

    private static void writeEvents(final Path filePath, final List<Event> events) throws IOException {
        try (final FileWriter writer = new FileWriter(filePath.toFile(), false)) {
            writer.append("TRANSACTION_ID,STATUS,MESSAGE\n");
            for (final var event : events) {
                writer.append(event.transactionId)
                        .append(",")
                        .append(event.status)
                        .append(",")
                        .append(event.message)
                        .append("\n");
            }
        }
    }

    private static User findValidUser(final Transaction transaction, final List<User> users) {
        for (final User user : users) {
            if (isUserValid(user, transaction)) {
                return user;
            }
        }
        return null;
    }

    private static boolean handleNonUniqueTransaction(final Transaction transaction, final List<String> usedTransactionIds, final Event event) {
        if (isTransIdUnique(transaction, usedTransactionIds)) {
            return false;
        }
        event.status = Event.STATUS_DECLINED;
        event.message = String.format("Transaction %s already processed (id non-unique)", transaction.transactionId);
        usedTransactionIds.add(transaction.transactionId);
        return true;
    }

    private static boolean handleNonExistingUser(final Transaction transaction, final User user, final Event event) {
        if (isUserExists(user)) {
            return false;
        }
        event.status = Event.STATUS_DECLINED;
        event.message = String.format("User %s not found in Users", transaction.userId);
        return true;
    }

    private static boolean handleFrozenUser(final User user, final Event event) {
        if (isUserFrozen(user)) {
            event.status = Event.STATUS_DECLINED;
            event.message = String.format("User %s is frozen", user.userId);
            return true;
        }
        return false;
    }

    private static boolean handleNegativeAmount(final Transaction transaction, final Event event) {
        if (isAmountNeg(transaction.amount)) {
            event.status = Event.STATUS_DECLINED;
            event.message = String.format("Amount %s is not positive", transaction.amount);
            return true;
        }
        return false;
    }

    private static boolean handleTransSmallerDepositLimit(final Transaction transaction, final User user, final Event event) {
        if (isTransSmallerDepositLimit(transaction, user)) {
            event.status = Event.STATUS_DECLINED;
            event.message = String.format("Amount %s is under the deposit limit of %s", transaction.amount, user.depositMin);
            return true;
        }
        return false;
    }

    private static boolean handleTransBiggerDepositLimit(final Transaction transaction, final User user, final Event event) {
        if (isTransBiggerDepositLimit(transaction, user)) {
            event.status = Event.STATUS_DECLINED;
            event.message = String.format("Amount %s is over the deposit limit of %s", transaction.amount, user.depositMax);
            return true;
        }
        return false;
    }

    private static boolean handleTransSmallerWithdrawLimit(final Transaction transaction, final User user, final Event event) {
        if (isTransSmallerWithdrawLimit(transaction, user)) {
            event.status = Event.STATUS_DECLINED;
            event.message = String.format("Amount %s is under the withdraw limit of %s", transaction.amount, user.withdrawMin);
            return true;
        }
        return false;
    }

    private static boolean handleTransBiggerWithdrawLimit(final Transaction transaction, final User user, final Event event) {
        if (isTransBiggerWithdrawLimit(transaction, user)) {
            event.status = Event.STATUS_DECLINED;
            event.message = String.format("Amount %s is over the withdraw limit of %s", transaction.amount, user.withdrawMax);
            return true;
        }
        return false;
    }

    private static boolean handleBalanceTooLow(final Transaction transaction, final User user, final Event event) {
        if (isBalanceEnough(transaction, user)) {
            return false;
        }
        event.status = Event.STATUS_DECLINED;
        event.message = String.format("Not enough balance to withdraw %s - balance is too low at %s", transaction.amount, user.balance);
        return true;
    }

    private static boolean handleNewAccount(final Transaction transaction, final Set<String> createdAccounts, final Event event) {
        if (isAccountNew(transaction, createdAccounts)) {
            event.status = Event.STATUS_DECLINED;
            event.message = String.format("Cannot withdraw with a new account %s", transaction.accountNumber);
            return true;
        }
        return false;
    }

    private static void handleBadTransType(final Event event) {
        event.status = Event.STATUS_DECLINED;
        event.message = "Invalid transaction type";
    }

    private static boolean handleInvalidIBAN(final Transaction transaction, final Event event) {
        final IBANReader ibanReader = new IBANReader();
        if (ibanReader.isValid(transaction.accountNumber)) {
            return false;
        }
        event.status = Event.STATUS_DECLINED;
        event.message = String.format("Invalid iban %s", transaction.accountNumber);
        return true;
    }

    private static boolean handleInvalidCountry(final Transaction transaction, final User user, final Event event) {
        if (isSameUserAndAccountCountry(transaction, user)) {
            return false;
        }
        event.status = Event.STATUS_DECLINED;
        event.message = String.format("Invalid account country %s; expected %s", transaction.accountNumber.substring(0, 2), user.country);
        return true;
    }

    private static boolean handleBadCardType(final Transaction transaction, final List<BinMapping> binMappings, final Event event) {
        if (Objects.equals(Objects.requireNonNull(Cards.findValidBin(transaction, binMappings)).type, Cards.TYPE_DC)) {
            return false;
        }
        event.status = Event.STATUS_DECLINED;
        event.message = "Only DC cards allowed; got CC";
        return true;
    }

    private static boolean handleInvalidCountry(final Transaction transaction, final User user, final List<BinMapping> binMappings, final Event event) {
        final CountryMap countryMap = new CountryMap();
        String expectedCountry = null;
        if (isSameUserAndAccountCountry(transaction, user, binMappings, countryMap)) {
            return false;
        }
        for (final Map.Entry<CountryThreeChar, CountryTwoChar> element : countryMap.countryThreeToTwoMap.entrySet()) {
            if (element.getValue().toString().equals(user.country)) {
                expectedCountry = element.getKey().toString();
                break;
            }
        }
        event.status = Event.STATUS_DECLINED;
        event.message = String.format("Invalid country %s; expected %s (%s)", Objects.requireNonNull(Cards.findValidBin(transaction, binMappings)).country, user.country, expectedCountry);
        return true;
    }

    private static void handleBadTransMethod(final Event event) {
        event.status = Event.STATUS_DECLINED;
        event.message = "Invalid payment method";
    }

    private static boolean handleAccountUsedByOther(final Transaction transaction, final User user, final Map<String, String> accountUserMap, final Event event) {
        if (isAccountHasUserAndUsersAreDiff(user, transaction, accountUserMap)) {
            event.status = Event.STATUS_DECLINED;
            event.message = String.format("Account %s is in use by other user", transaction.accountNumber);
            return true;
        }
        return false;
    }

    private static void handleSuccess(final Transaction transaction, final User user, final Set<String> createdAccounts, final Map<String, String> accountUserMap, final Event event) throws Exception {
        if (isMoneyAmountInvalid(user, transaction)) {
            throw new Exception("Money amount is more than 20 digits.");
        }
        if (isTransTypeWithdraw(transaction)) {
            user.balance = user.balance.subtract(transaction.amount);
        } else {
            user.balance = user.balance.add(transaction.amount);
            createdAccounts.add(transaction.accountNumber);
        }
        event.status = Event.STATUS_APPROVED;
        event.message = "OK";
        accountUserMap.put(transaction.accountNumber, transaction.userId);
    }

    private static boolean isUserValid(final User user, final Transaction transaction) {
        return user.userId.equals(transaction.userId);
    }

    private static boolean isMoneyAmountInvalid(final User user, final Transaction transaction) {
        final int MONEY_LENGTH_LIMIT = 21;
        return user.balance.add(transaction.amount).toString().length() > MONEY_LENGTH_LIMIT;
    }

    private static boolean isUserExists(final User user) {
        return user != null;
    }

    private static boolean isUserFrozen(final User user) {
        return user.frozen == 1;
    }

    private static boolean isTransIdUnique(final Transaction transaction, final List<String> usedTransactionIds) {
        return !usedTransactionIds.contains(transaction.transactionId);
    }

    private static boolean isAccountHasUserAndUsersAreDiff(final User user, final Transaction transaction, final Map<String, String> accountUserMap) {
        final String ownerId = accountUserMap.get(transaction.accountNumber);
        return ownerId != null && !ownerId.equals(user.userId);
    }

    private static boolean isAmountNeg(final BigDecimal amount) {
        return amount.compareTo(new BigDecimal(0)) < 0;
    }

    private static boolean isTransTypeDeposit(final Transaction transaction) {
        return transaction.type.equals(Transaction.TYPE_DEPOSIT);
    }

    private static boolean isTransTypeWithdraw(final Transaction transaction) {
        return transaction.type.equals(Transaction.TYPE_WITHDRAW);
    }

    private static boolean isTransSmallerDepositLimit(final Transaction transaction, final User user) {
        return transaction.amount.compareTo(user.depositMin) < 0;
    }

    private static boolean isTransBiggerDepositLimit(final Transaction transaction, final User user) {
        return transaction.amount.compareTo(user.depositMax) > 0;
    }

    private static boolean isTransSmallerWithdrawLimit(final Transaction transaction, final User user) {
        return transaction.amount.compareTo(user.withdrawMin) < 0;
    }

    private static boolean isTransBiggerWithdrawLimit(final Transaction transaction, final User user) {
        return transaction.amount.compareTo(user.withdrawMax) > 0;
    }

    private static boolean isBalanceEnough(final Transaction transaction, final User user) {
        return user.balance.compareTo(transaction.amount) >= 0;
    }

    private static boolean isAccountNew(final Transaction transaction, final Set<String> createdAccounts) {
        return !createdAccounts.contains(transaction.accountNumber);
    }

    private static boolean isTransMethodTransfer(final Transaction transaction) {
        return transaction.method.equals(Transaction.METHOD_TRANSFER);
    }

    private static boolean isTransMethodCard(final Transaction transaction) {
        return transaction.method.equals(Transaction.METHOD_CARD);
    }

    private static boolean isSameUserAndAccountCountry(final Transaction transaction, final User user) {
        return user.country.equals(transaction.accountNumber.substring(0, 2));
    }

    private static boolean isSameUserAndAccountCountry(final Transaction transaction, final User user, final List<BinMapping> binMappings, final CountryMap countryMap) {
        return user.country.equals(countryMap.countryThreeToTwoMap.get(
                CountryThreeChar.valueOf(Objects.requireNonNull(Cards.findValidBin(transaction, binMappings)).country)
        ).toString());
    }
}

class User {
    public String userId;
    public String username;
    public BigDecimal balance;
    public String country;
    public int frozen;
    public BigDecimal depositMin;
    public BigDecimal depositMax;
    public BigDecimal withdrawMin;
    public BigDecimal withdrawMax;

    User(String userId, String username, double balance, String country, int frozen, double depositMin, double depositMax, double withdrawMin, double withdrawMax) {
        this.userId = userId;
        this.username = username;
        this.balance = new BigDecimal(balance).setScale(2, BigDecimal.ROUND_HALF_UP);
        this.country = country;
        this.frozen = frozen;
        this.depositMin = new BigDecimal(depositMin).setScale(2, BigDecimal.ROUND_HALF_UP);
        this.depositMax = new BigDecimal(depositMax).setScale(2, BigDecimal.ROUND_HALF_UP);
        this.withdrawMin = new BigDecimal(withdrawMin).setScale(2, BigDecimal.ROUND_HALF_UP);
        this.withdrawMax = new BigDecimal(withdrawMax).setScale(2, BigDecimal.ROUND_HALF_UP);
    }
}

class Transaction {
    public static final String METHOD_TRANSFER = "TRANSFER";
    public static final String METHOD_CARD = "CARD";
    public static final String TYPE_DEPOSIT = "DEPOSIT";
    public static final String TYPE_WITHDRAW = "WITHDRAW";
    public String transactionId;
    public String userId;
    public String type;
    public BigDecimal amount;
    public String method;
    public String accountNumber;

    Transaction(String transactionId, String userId, String type, String amount, String method, String accountNumber) {
        this.transactionId = transactionId;
        this.userId = userId;
        this.type = type;
        this.amount = new BigDecimal(amount).setScale(2, BigDecimal.ROUND_HALF_UP);
        this.method = method;
        this.accountNumber = accountNumber;
    }
}

class BinMapping {
    public String name;
    public long rangeFrom;
    public long rangeTo;
    public String type;
    public String country;

    public BinMapping(String name, long rangeFrom, long rangeTo, String type, String country) {
        this.name = name;
        this.rangeFrom = rangeFrom;
        this.rangeTo = rangeTo;
        this.type = type;
        this.country = country;
    }
}

class Event {
    public static final String STATUS_DECLINED = "DECLINED";
    public static final String STATUS_APPROVED = "APPROVED";
    public String transactionId;
    public String status;
    public String message;

    Event(String transactionId) {
        this.transactionId = transactionId;
    }
}

class Cards {
    public static final String TYPE_DC = "DC";
    public static final String TYPE_CC = "CC";

    public static BinMapping findValidBin(final Transaction transaction, final List<BinMapping> binMappingList) {
        double accountNumber = Double.parseDouble(transaction.accountNumber.substring(0, 10));
        int index = binarySearch(accountNumber, binMappingList, 0, binMappingList.size() - 1);
        BinMapping binMapping = binMappingList.get(index);
        if (isValidBin(accountNumber, binMapping)) {
            return binMapping;
        }
        return null;
    }

    private static int binarySearch(final double searchedElement, final List<BinMapping> binMappingList, final int low, final int high) {
        int needle = low + (high - low) / 2;
        if (binMappingList.get(needle).rangeFrom < searchedElement && high - low != 1) {
            needle = binarySearch(
                    searchedElement,
                    binMappingList,
                    needle,
                    high
            );
        } else if (binMappingList.get(needle).rangeFrom > searchedElement && high - low != 1) {
            needle = binarySearch(
                    searchedElement,
                    binMappingList,
                    low,
                    needle
            );
        }
        return needle;
    }

    private static boolean isValidBin(final double accountNumber, final BinMapping binMapping) {
        return accountNumber - binMapping.rangeFrom <= binMapping.rangeTo - binMapping.rangeFrom;
    }
}

class IBANReader {
    private final Map<CountryTwoChar, Integer> IBANLengthsMap;
    private final Map<String, Integer> alphabetMap;

    IBANReader() {
        this.IBANLengthsMap = createIBANLengthsMap();
        this.alphabetMap = createAlphabetMap();
    }

    public boolean isValid(final String accountNumber) {
        final CountryTwoChar key = CountryTwoChar.valueOf(accountNumber.substring(0, 2));
        if (IBANLengthsMap.get(key) != accountNumber.length()) {
            return false;
        }
        final String shiftedString = accountNumber.substring(0, 4);
        String shiftedAccountNum = accountNumber.substring(4) + shiftedString;
        for (String element : alphabetMap.keySet()) {
            shiftedAccountNum = shiftedAccountNum.replaceAll(element, alphabetMap.get(element).toString());
        }
        BigInteger intAccountNumber = new BigInteger(shiftedAccountNum);
        return intAccountNumber.remainder(new BigInteger("97")).intValue() == 1;
    }

    private Map<String, Integer> createAlphabetMap() {
        Map<String, Integer> answer = new HashMap<>();
        int startValue = 10;
        int numberOfLetters = 26;
        char currentChar;
        for (int i = 0; i < numberOfLetters; i++) {
            currentChar = Character.forDigit(i + startValue, 36);
            answer.put(String.valueOf(currentChar).toUpperCase(), i + startValue);
        }
        return answer;
    }

    private Map<CountryTwoChar, Integer> createIBANLengthsMap() {
        return Map.<CountryTwoChar, Integer>ofEntries(
                Map.entry(CountryTwoChar.AD, 24),
                Map.entry(CountryTwoChar.AE, 23),
                Map.entry(CountryTwoChar.AL, 28),
                Map.entry(CountryTwoChar.AO, 25),
                Map.entry(CountryTwoChar.AT, 20),
                Map.entry(CountryTwoChar.AZ, 28),
                Map.entry(CountryTwoChar.BA, 20),
                Map.entry(CountryTwoChar.BE, 16),
                Map.entry(CountryTwoChar.BF, 28),
                Map.entry(CountryTwoChar.BG, 22),
                Map.entry(CountryTwoChar.BH, 22),
                Map.entry(CountryTwoChar.BI, 27),
                Map.entry(CountryTwoChar.BJ, 28),
                Map.entry(CountryTwoChar.BR, 29),
                Map.entry(CountryTwoChar.BY, 28),
                Map.entry(CountryTwoChar.CF, 27),
                Map.entry(CountryTwoChar.CG, 27),
                Map.entry(CountryTwoChar.CH, 21),
                Map.entry(CountryTwoChar.CI, 28),
                Map.entry(CountryTwoChar.CM, 27),
                Map.entry(CountryTwoChar.CR, 22),
                Map.entry(CountryTwoChar.CV, 25),
                Map.entry(CountryTwoChar.CY, 28),
                Map.entry(CountryTwoChar.CZ, 24),
                Map.entry(CountryTwoChar.DE, 22),
                Map.entry(CountryTwoChar.DJ, 27),
                Map.entry(CountryTwoChar.DK, 18),
                Map.entry(CountryTwoChar.DO, 28),
                Map.entry(CountryTwoChar.DZ, 26),
                Map.entry(CountryTwoChar.EE, 20),
                Map.entry(CountryTwoChar.EG, 29),
                Map.entry(CountryTwoChar.ES, 24),
                Map.entry(CountryTwoChar.FI, 18),
                Map.entry(CountryTwoChar.FK, 22),
                Map.entry(CountryTwoChar.FO, 18),
                Map.entry(CountryTwoChar.FR, 27),
                Map.entry(CountryTwoChar.GA, 27),
                Map.entry(CountryTwoChar.GB, 22),
                Map.entry(CountryTwoChar.GE, 22),
                Map.entry(CountryTwoChar.GI, 23),
                Map.entry(CountryTwoChar.GL, 18),
                Map.entry(CountryTwoChar.GQ, 27),
                Map.entry(CountryTwoChar.GR, 27),
                Map.entry(CountryTwoChar.GT, 28),
                Map.entry(CountryTwoChar.GW, 25),
                Map.entry(CountryTwoChar.HN, 28),
                Map.entry(CountryTwoChar.HR, 21),
                Map.entry(CountryTwoChar.HU, 28),
                Map.entry(CountryTwoChar.IE, 22),
                Map.entry(CountryTwoChar.IL, 23),
                Map.entry(CountryTwoChar.IQ, 23),
                Map.entry(CountryTwoChar.IR, 26),
                Map.entry(CountryTwoChar.IS, 26),
                Map.entry(CountryTwoChar.IT, 27),
                Map.entry(CountryTwoChar.JO, 30),
                Map.entry(CountryTwoChar.KM, 27),
                Map.entry(CountryTwoChar.KW, 30),
                Map.entry(CountryTwoChar.KZ, 20),
                Map.entry(CountryTwoChar.LB, 28),
                Map.entry(CountryTwoChar.LC, 32),
                Map.entry(CountryTwoChar.LI, 21),
                Map.entry(CountryTwoChar.LT, 20),
                Map.entry(CountryTwoChar.LU, 20),
                Map.entry(CountryTwoChar.LV, 21),
                Map.entry(CountryTwoChar.LY, 25),
                Map.entry(CountryTwoChar.MA, 28),
                Map.entry(CountryTwoChar.MC, 27),
                Map.entry(CountryTwoChar.MD, 24),
                Map.entry(CountryTwoChar.ME, 22),
                Map.entry(CountryTwoChar.MG, 27),
                Map.entry(CountryTwoChar.MK, 19),
                Map.entry(CountryTwoChar.ML, 28),
                Map.entry(CountryTwoChar.MN, 22),
                Map.entry(CountryTwoChar.MR, 27),
                Map.entry(CountryTwoChar.MT, 31),
                Map.entry(CountryTwoChar.MU, 30),
                Map.entry(CountryTwoChar.MZ, 25),
                Map.entry(CountryTwoChar.NE, 28),
                Map.entry(CountryTwoChar.NI, 32),
                Map.entry(CountryTwoChar.NL, 18),
                Map.entry(CountryTwoChar.NO, 15),
                Map.entry(CountryTwoChar.OM, 22),
                Map.entry(CountryTwoChar.PK, 24),
                Map.entry(CountryTwoChar.PL, 28),
                Map.entry(CountryTwoChar.PS, 29),
                Map.entry(CountryTwoChar.PT, 25),
                Map.entry(CountryTwoChar.QA, 29),
                Map.entry(CountryTwoChar.RO, 24),
                Map.entry(CountryTwoChar.RS, 22),
                Map.entry(CountryTwoChar.RU, 33),
                Map.entry(CountryTwoChar.SA, 24),
                Map.entry(CountryTwoChar.SC, 31),
                Map.entry(CountryTwoChar.SD, 18),
                Map.entry(CountryTwoChar.SE, 24),
                Map.entry(CountryTwoChar.SI, 19),
                Map.entry(CountryTwoChar.SK, 24),
                Map.entry(CountryTwoChar.SM, 27),
                Map.entry(CountryTwoChar.SN, 28),
                Map.entry(CountryTwoChar.SO, 22),
                Map.entry(CountryTwoChar.ST, 25),
                Map.entry(CountryTwoChar.SV, 28),
                Map.entry(CountryTwoChar.TD, 27),
                Map.entry(CountryTwoChar.TG, 28),
                Map.entry(CountryTwoChar.TL, 23),
                Map.entry(CountryTwoChar.TN, 24),
                Map.entry(CountryTwoChar.TR, 26),
                Map.entry(CountryTwoChar.UA, 29),
                Map.entry(CountryTwoChar.VA, 22),
                Map.entry(CountryTwoChar.VG, 24),
                Map.entry(CountryTwoChar.XK, 20)
        );
    }
}

class CountryMap {
    public final Map<CountryThreeChar, CountryTwoChar> countryThreeToTwoMap = createCountryThreeToTwoMap();

    private Map<CountryThreeChar, CountryTwoChar> createCountryThreeToTwoMap() {
        return Map.<CountryThreeChar, CountryTwoChar>ofEntries(
                Map.entry(CountryThreeChar.AND, CountryTwoChar.AD),
                Map.entry(CountryThreeChar.ARE, CountryTwoChar.AE),
                Map.entry(CountryThreeChar.AFG, CountryTwoChar.AF),
                Map.entry(CountryThreeChar.ATG, CountryTwoChar.AG),
                Map.entry(CountryThreeChar.AIA, CountryTwoChar.AI),
                Map.entry(CountryThreeChar.ALB, CountryTwoChar.AL),
                Map.entry(CountryThreeChar.ARM, CountryTwoChar.AM),
                Map.entry(CountryThreeChar.AGO, CountryTwoChar.AO),
                Map.entry(CountryThreeChar.ATA, CountryTwoChar.AQ),
                Map.entry(CountryThreeChar.ARG, CountryTwoChar.AR),
                Map.entry(CountryThreeChar.ASM, CountryTwoChar.AS),
                Map.entry(CountryThreeChar.AUT, CountryTwoChar.AT),
                Map.entry(CountryThreeChar.AUS, CountryTwoChar.AU),
                Map.entry(CountryThreeChar.ABW, CountryTwoChar.AW),
                Map.entry(CountryThreeChar.ALA, CountryTwoChar.AX),
                Map.entry(CountryThreeChar.AZE, CountryTwoChar.AZ),
                Map.entry(CountryThreeChar.BIH, CountryTwoChar.BA),
                Map.entry(CountryThreeChar.BRB, CountryTwoChar.BB),
                Map.entry(CountryThreeChar.BGD, CountryTwoChar.BD),
                Map.entry(CountryThreeChar.BEL, CountryTwoChar.BE),
                Map.entry(CountryThreeChar.BFA, CountryTwoChar.BF),
                Map.entry(CountryThreeChar.BGR, CountryTwoChar.BG),
                Map.entry(CountryThreeChar.BHR, CountryTwoChar.BH),
                Map.entry(CountryThreeChar.BDI, CountryTwoChar.BI),
                Map.entry(CountryThreeChar.BEN, CountryTwoChar.BJ),
                Map.entry(CountryThreeChar.BLM, CountryTwoChar.BL),
                Map.entry(CountryThreeChar.BMU, CountryTwoChar.BM),
                Map.entry(CountryThreeChar.BRN, CountryTwoChar.BN),
                Map.entry(CountryThreeChar.BOL, CountryTwoChar.BO),
                Map.entry(CountryThreeChar.BES, CountryTwoChar.BQ),
                Map.entry(CountryThreeChar.BRA, CountryTwoChar.BR),
                Map.entry(CountryThreeChar.BHS, CountryTwoChar.BS),
                Map.entry(CountryThreeChar.BTN, CountryTwoChar.BT),
                Map.entry(CountryThreeChar.BVT, CountryTwoChar.BV),
                Map.entry(CountryThreeChar.BWA, CountryTwoChar.BW),
                Map.entry(CountryThreeChar.BLR, CountryTwoChar.BY),
                Map.entry(CountryThreeChar.BLZ, CountryTwoChar.BZ),
                Map.entry(CountryThreeChar.CAN, CountryTwoChar.CA),
                Map.entry(CountryThreeChar.CCK, CountryTwoChar.CC),
                Map.entry(CountryThreeChar.COD, CountryTwoChar.CD),
                Map.entry(CountryThreeChar.CAF, CountryTwoChar.CF),
                Map.entry(CountryThreeChar.COG, CountryTwoChar.CG),
                Map.entry(CountryThreeChar.CHE, CountryTwoChar.CH),
                Map.entry(CountryThreeChar.CIV, CountryTwoChar.CI),
                Map.entry(CountryThreeChar.COK, CountryTwoChar.CK),
                Map.entry(CountryThreeChar.CHL, CountryTwoChar.CL),
                Map.entry(CountryThreeChar.CMR, CountryTwoChar.CM),
                Map.entry(CountryThreeChar.CHN, CountryTwoChar.CN),
                Map.entry(CountryThreeChar.COL, CountryTwoChar.CO),
                Map.entry(CountryThreeChar.CRI, CountryTwoChar.CR),
                Map.entry(CountryThreeChar.CUB, CountryTwoChar.CU),
                Map.entry(CountryThreeChar.CPV, CountryTwoChar.CV),
                Map.entry(CountryThreeChar.CUW, CountryTwoChar.CW),
                Map.entry(CountryThreeChar.CXR, CountryTwoChar.CX),
                Map.entry(CountryThreeChar.CYP, CountryTwoChar.CY),
                Map.entry(CountryThreeChar.CZE, CountryTwoChar.CZ),
                Map.entry(CountryThreeChar.DEU, CountryTwoChar.DE),
                Map.entry(CountryThreeChar.DJI, CountryTwoChar.DJ),
                Map.entry(CountryThreeChar.DNK, CountryTwoChar.DK),
                Map.entry(CountryThreeChar.DMA, CountryTwoChar.DM),
                Map.entry(CountryThreeChar.DOM, CountryTwoChar.DO),
                Map.entry(CountryThreeChar.DZA, CountryTwoChar.DZ),
                Map.entry(CountryThreeChar.ECU, CountryTwoChar.EC),
                Map.entry(CountryThreeChar.EST, CountryTwoChar.EE),
                Map.entry(CountryThreeChar.EGY, CountryTwoChar.EG),
                Map.entry(CountryThreeChar.ESH, CountryTwoChar.EH),
                Map.entry(CountryThreeChar.ERI, CountryTwoChar.ER),
                Map.entry(CountryThreeChar.ESP, CountryTwoChar.ES),
                Map.entry(CountryThreeChar.ETH, CountryTwoChar.ET),
                Map.entry(CountryThreeChar.FIN, CountryTwoChar.FI),
                Map.entry(CountryThreeChar.FJI, CountryTwoChar.FJ),
                Map.entry(CountryThreeChar.FLK, CountryTwoChar.FK),
                Map.entry(CountryThreeChar.FSM, CountryTwoChar.FM),
                Map.entry(CountryThreeChar.FRO, CountryTwoChar.FO),
                Map.entry(CountryThreeChar.FRA, CountryTwoChar.FR),
                Map.entry(CountryThreeChar.GAB, CountryTwoChar.GA),
                Map.entry(CountryThreeChar.GBR, CountryTwoChar.GB),
                Map.entry(CountryThreeChar.GRD, CountryTwoChar.GD),
                Map.entry(CountryThreeChar.GEO, CountryTwoChar.GE),
                Map.entry(CountryThreeChar.GUF, CountryTwoChar.GF),
                Map.entry(CountryThreeChar.GGY, CountryTwoChar.GG),
                Map.entry(CountryThreeChar.GHA, CountryTwoChar.GH),
                Map.entry(CountryThreeChar.GIB, CountryTwoChar.GI),
                Map.entry(CountryThreeChar.GRL, CountryTwoChar.GL),
                Map.entry(CountryThreeChar.GMB, CountryTwoChar.GM),
                Map.entry(CountryThreeChar.GIN, CountryTwoChar.GN),
                Map.entry(CountryThreeChar.GLP, CountryTwoChar.GP),
                Map.entry(CountryThreeChar.GNQ, CountryTwoChar.GQ),
                Map.entry(CountryThreeChar.GRC, CountryTwoChar.GR),
                Map.entry(CountryThreeChar.SGS, CountryTwoChar.GS),
                Map.entry(CountryThreeChar.GTM, CountryTwoChar.GT),
                Map.entry(CountryThreeChar.GUM, CountryTwoChar.GU),
                Map.entry(CountryThreeChar.GNB, CountryTwoChar.GW),
                Map.entry(CountryThreeChar.GUY, CountryTwoChar.GY),
                Map.entry(CountryThreeChar.HKG, CountryTwoChar.HK),
                Map.entry(CountryThreeChar.HMD, CountryTwoChar.HM),
                Map.entry(CountryThreeChar.HND, CountryTwoChar.HN),
                Map.entry(CountryThreeChar.HRV, CountryTwoChar.HR),
                Map.entry(CountryThreeChar.HTI, CountryTwoChar.HT),
                Map.entry(CountryThreeChar.HUN, CountryTwoChar.HU),
                Map.entry(CountryThreeChar.IDN, CountryTwoChar.ID),
                Map.entry(CountryThreeChar.IRL, CountryTwoChar.IE),
                Map.entry(CountryThreeChar.ISR, CountryTwoChar.IL),
                Map.entry(CountryThreeChar.IMN, CountryTwoChar.IM),
                Map.entry(CountryThreeChar.IND, CountryTwoChar.IN),
                Map.entry(CountryThreeChar.IOT, CountryTwoChar.IO),
                Map.entry(CountryThreeChar.IRQ, CountryTwoChar.IQ),
                Map.entry(CountryThreeChar.IRN, CountryTwoChar.IR),
                Map.entry(CountryThreeChar.ISL, CountryTwoChar.IS),
                Map.entry(CountryThreeChar.ITA, CountryTwoChar.IT),
                Map.entry(CountryThreeChar.JEY, CountryTwoChar.JE),
                Map.entry(CountryThreeChar.JAM, CountryTwoChar.JM),
                Map.entry(CountryThreeChar.JOR, CountryTwoChar.JO),
                Map.entry(CountryThreeChar.JPN, CountryTwoChar.JP),
                Map.entry(CountryThreeChar.KEN, CountryTwoChar.KE),
                Map.entry(CountryThreeChar.KGZ, CountryTwoChar.KG),
                Map.entry(CountryThreeChar.KHM, CountryTwoChar.KH),
                Map.entry(CountryThreeChar.KIR, CountryTwoChar.KI),
                Map.entry(CountryThreeChar.COM, CountryTwoChar.KM),
                Map.entry(CountryThreeChar.KNA, CountryTwoChar.KN),
                Map.entry(CountryThreeChar.PRK, CountryTwoChar.KP),
                Map.entry(CountryThreeChar.KOR, CountryTwoChar.KR),
                Map.entry(CountryThreeChar.KWT, CountryTwoChar.KW),
                Map.entry(CountryThreeChar.CYM, CountryTwoChar.KY),
                Map.entry(CountryThreeChar.KAZ, CountryTwoChar.KZ),
                Map.entry(CountryThreeChar.LAO, CountryTwoChar.LA),
                Map.entry(CountryThreeChar.LBN, CountryTwoChar.LB),
                Map.entry(CountryThreeChar.LCA, CountryTwoChar.LC),
                Map.entry(CountryThreeChar.LIE, CountryTwoChar.LI),
                Map.entry(CountryThreeChar.LKA, CountryTwoChar.LK),
                Map.entry(CountryThreeChar.LBR, CountryTwoChar.LR),
                Map.entry(CountryThreeChar.LSO, CountryTwoChar.LS),
                Map.entry(CountryThreeChar.LTU, CountryTwoChar.LT),
                Map.entry(CountryThreeChar.LUX, CountryTwoChar.LU),
                Map.entry(CountryThreeChar.LVA, CountryTwoChar.LV),
                Map.entry(CountryThreeChar.LBY, CountryTwoChar.LY),
                Map.entry(CountryThreeChar.MAR, CountryTwoChar.MA),
                Map.entry(CountryThreeChar.MCO, CountryTwoChar.MC),
                Map.entry(CountryThreeChar.MDA, CountryTwoChar.MD),
                Map.entry(CountryThreeChar.MNE, CountryTwoChar.ME),
                Map.entry(CountryThreeChar.MAF, CountryTwoChar.MF),
                Map.entry(CountryThreeChar.MDG, CountryTwoChar.MG),
                Map.entry(CountryThreeChar.MHL, CountryTwoChar.MH),
                Map.entry(CountryThreeChar.MKD, CountryTwoChar.MK),
                Map.entry(CountryThreeChar.MLI, CountryTwoChar.ML),
                Map.entry(CountryThreeChar.MMR, CountryTwoChar.MM),
                Map.entry(CountryThreeChar.MNG, CountryTwoChar.MN),
                Map.entry(CountryThreeChar.MAC, CountryTwoChar.MO),
                Map.entry(CountryThreeChar.MNP, CountryTwoChar.MP),
                Map.entry(CountryThreeChar.MTQ, CountryTwoChar.MQ),
                Map.entry(CountryThreeChar.MRT, CountryTwoChar.MR),
                Map.entry(CountryThreeChar.MSR, CountryTwoChar.MS),
                Map.entry(CountryThreeChar.MLT, CountryTwoChar.MT),
                Map.entry(CountryThreeChar.MUS, CountryTwoChar.MU),
                Map.entry(CountryThreeChar.MDV, CountryTwoChar.MV),
                Map.entry(CountryThreeChar.MWI, CountryTwoChar.MW),
                Map.entry(CountryThreeChar.MEX, CountryTwoChar.MX),
                Map.entry(CountryThreeChar.MYS, CountryTwoChar.MY),
                Map.entry(CountryThreeChar.MOZ, CountryTwoChar.MZ),
                Map.entry(CountryThreeChar.NAM, CountryTwoChar.NA),
                Map.entry(CountryThreeChar.NCL, CountryTwoChar.NC),
                Map.entry(CountryThreeChar.NER, CountryTwoChar.NE),
                Map.entry(CountryThreeChar.NFK, CountryTwoChar.NF),
                Map.entry(CountryThreeChar.NGA, CountryTwoChar.NG),
                Map.entry(CountryThreeChar.NIC, CountryTwoChar.NI),
                Map.entry(CountryThreeChar.NLD, CountryTwoChar.NL),
                Map.entry(CountryThreeChar.NOR, CountryTwoChar.NO),
                Map.entry(CountryThreeChar.NPL, CountryTwoChar.NP),
                Map.entry(CountryThreeChar.NRU, CountryTwoChar.NR),
                Map.entry(CountryThreeChar.NIU, CountryTwoChar.NU),
                Map.entry(CountryThreeChar.NZL, CountryTwoChar.NZ),
                Map.entry(CountryThreeChar.OMN, CountryTwoChar.OM),
                Map.entry(CountryThreeChar.PAN, CountryTwoChar.PA),
                Map.entry(CountryThreeChar.PER, CountryTwoChar.PE),
                Map.entry(CountryThreeChar.PYF, CountryTwoChar.PF),
                Map.entry(CountryThreeChar.PNG, CountryTwoChar.PG),
                Map.entry(CountryThreeChar.PHL, CountryTwoChar.PH),
                Map.entry(CountryThreeChar.PAK, CountryTwoChar.PK),
                Map.entry(CountryThreeChar.POL, CountryTwoChar.PL),
                Map.entry(CountryThreeChar.SPM, CountryTwoChar.PM),
                Map.entry(CountryThreeChar.PCN, CountryTwoChar.PN),
                Map.entry(CountryThreeChar.PRI, CountryTwoChar.PR),
                Map.entry(CountryThreeChar.PSE, CountryTwoChar.PS),
                Map.entry(CountryThreeChar.PRT, CountryTwoChar.PT),
                Map.entry(CountryThreeChar.PLW, CountryTwoChar.PW),
                Map.entry(CountryThreeChar.PRY, CountryTwoChar.PY),
                Map.entry(CountryThreeChar.QAT, CountryTwoChar.QA),
                Map.entry(CountryThreeChar.REU, CountryTwoChar.RE),
                Map.entry(CountryThreeChar.ROU, CountryTwoChar.RO),
                Map.entry(CountryThreeChar.SRB, CountryTwoChar.RS),
                Map.entry(CountryThreeChar.RUS, CountryTwoChar.RU),
                Map.entry(CountryThreeChar.RWA, CountryTwoChar.RW),
                Map.entry(CountryThreeChar.SAU, CountryTwoChar.SA),
                Map.entry(CountryThreeChar.SLB, CountryTwoChar.SB),
                Map.entry(CountryThreeChar.SYC, CountryTwoChar.SC),
                Map.entry(CountryThreeChar.SDN, CountryTwoChar.SD),
                Map.entry(CountryThreeChar.SWE, CountryTwoChar.SE),
                Map.entry(CountryThreeChar.SGP, CountryTwoChar.SG),
                Map.entry(CountryThreeChar.SHN, CountryTwoChar.SH),
                Map.entry(CountryThreeChar.SVN, CountryTwoChar.SI),
                Map.entry(CountryThreeChar.SJM, CountryTwoChar.SJ),
                Map.entry(CountryThreeChar.SVK, CountryTwoChar.SK),
                Map.entry(CountryThreeChar.SLE, CountryTwoChar.SL),
                Map.entry(CountryThreeChar.SMR, CountryTwoChar.SM),
                Map.entry(CountryThreeChar.SEN, CountryTwoChar.SN),
                Map.entry(CountryThreeChar.SOM, CountryTwoChar.SO),
                Map.entry(CountryThreeChar.SUR, CountryTwoChar.SR),
                Map.entry(CountryThreeChar.SSD, CountryTwoChar.SS),
                Map.entry(CountryThreeChar.STP, CountryTwoChar.ST),
                Map.entry(CountryThreeChar.SLV, CountryTwoChar.SV),
                Map.entry(CountryThreeChar.SXM, CountryTwoChar.SX),
                Map.entry(CountryThreeChar.SYR, CountryTwoChar.SY),
                Map.entry(CountryThreeChar.SWZ, CountryTwoChar.SZ),
                Map.entry(CountryThreeChar.TCA, CountryTwoChar.TC),
                Map.entry(CountryThreeChar.TCD, CountryTwoChar.TD),
                Map.entry(CountryThreeChar.ATF, CountryTwoChar.TF),
                Map.entry(CountryThreeChar.TGO, CountryTwoChar.TG),
                Map.entry(CountryThreeChar.THA, CountryTwoChar.TH),
                Map.entry(CountryThreeChar.TJK, CountryTwoChar.TJ),
                Map.entry(CountryThreeChar.TKL, CountryTwoChar.TK),
                Map.entry(CountryThreeChar.TLS, CountryTwoChar.TL),
                Map.entry(CountryThreeChar.TKM, CountryTwoChar.TM),
                Map.entry(CountryThreeChar.TUN, CountryTwoChar.TN),
                Map.entry(CountryThreeChar.TON, CountryTwoChar.TO),
                Map.entry(CountryThreeChar.TUR, CountryTwoChar.TR),
                Map.entry(CountryThreeChar.TTO, CountryTwoChar.TT),
                Map.entry(CountryThreeChar.TUV, CountryTwoChar.TV),
                Map.entry(CountryThreeChar.TWN, CountryTwoChar.TW),
                Map.entry(CountryThreeChar.TZA, CountryTwoChar.TZ),
                Map.entry(CountryThreeChar.UKR, CountryTwoChar.UA),
                Map.entry(CountryThreeChar.UGA, CountryTwoChar.UG),
                Map.entry(CountryThreeChar.UMI, CountryTwoChar.UM),
                Map.entry(CountryThreeChar.USA, CountryTwoChar.US),
                Map.entry(CountryThreeChar.URY, CountryTwoChar.UY),
                Map.entry(CountryThreeChar.UZB, CountryTwoChar.UZ),
                Map.entry(CountryThreeChar.VAT, CountryTwoChar.VA),
                Map.entry(CountryThreeChar.VCT, CountryTwoChar.VC),
                Map.entry(CountryThreeChar.VEN, CountryTwoChar.VE),
                Map.entry(CountryThreeChar.VGB, CountryTwoChar.VG),
                Map.entry(CountryThreeChar.VIR, CountryTwoChar.VI),
                Map.entry(CountryThreeChar.VNM, CountryTwoChar.VN),
                Map.entry(CountryThreeChar.VUT, CountryTwoChar.VU),
                Map.entry(CountryThreeChar.WLF, CountryTwoChar.WF),
                Map.entry(CountryThreeChar.WSM, CountryTwoChar.WS),
                Map.entry(CountryThreeChar.YEM, CountryTwoChar.YE),
                Map.entry(CountryThreeChar.MYT, CountryTwoChar.YT),
                Map.entry(CountryThreeChar.ZAF, CountryTwoChar.ZA),
                Map.entry(CountryThreeChar.ZMB, CountryTwoChar.ZM),
                Map.entry(CountryThreeChar.ZWE, CountryTwoChar.ZW)
        );
    }
}

enum CountryTwoChar {
    AL, AD, AT, AZ, BH, BY, BE, BA, BR, BG, CR, HR, CY, CZ, DK, DO, TL, EG, SV, EE, FK, FO, FI, FR, GE, DE, GI, GR, GL, GT, HU, IS, IQ, IE, IL, IT, JO, KZ, XK, KW, LV, LB, LY, LI, LT, LU, MT, MR, MU, MD, MC, MN, ME, NL, MK, NO, OM, PK, PS, PL, QA, RO, RU, LC, SM, ST, SA, RS, CS, SK, SI, SO, ES, SD, SE, CH, TN, TR, UA, AE, GB, VA, CI, SC, SN, PT, US, SG, CA, JP, IN, TG, TD, NI, NE, MZ, ML, MG, MA, KM, IR, HN, GW, GQ, GA, DZ, DJ, CV, CM, CG, CF, BJ, BI, BF, AO, AG, AF, VG, AI, AM, AQ, AR, AS, AU, AW, AX, BB, BD, BL, BM, BN, BO, BQ, BS, BT, BV, BW, BZ, CC, CD, CK, CL, CN, CO, CU, CW, CX, DM, EC, EH, ER, ET, FJ, FM, GD, GF, GG, GH, GM, GN, GP, GS, GU, GY, HK, HM, HT, ID, IM, IO, JE, JM, KE, KG, KH, KI, KN, KP, KR, KY, LA, LK, LR, LS, MF, MH, MM, MO, MP, MQ, MS, MV, MW, MX, MY, NA, NC, NF, NG, NP, NR, NU, NZ, PA, PE, PF, PG, PH, PM, PN, PR, PW, PY, RE, RW, SB, SH, SJ, SL, SR, SS, SX, SY, SZ, TC, TF, TH, TJ, TK, TM, TO, TT, TV, TW, TZ, UG, UM, UY, UZ, VC, VE, VI, VN, VU, WF, WS, YE, YT, ZA, ZM, ZW
}

enum CountryThreeChar {
    AND, ARE, ALB, AUT, AZE, BIH, BEL, BGR, BHR, BRA, BLR, CHE, CRI, CYP, CZE, DEU, DNK, DOM, EST, EGY, ESP, FIN, FLK, FRO, FRA, GBR, GEO, GIB, GRL, GRC, GTM, HRV, HUN, IRL, ISR, IRQ, ISL, ITA, JOR, KWT, KAZ, LBN, LCA, LIE, LTU, LUX, LVA, LBY, MCO, MDA, MNE, MKD, MNG, MRT, MLT, MUS, NLD, NOR, OMN, PAK, POL, PSE, PRT, QAT, ROU, SRB, RUS, SAU, SYC, SDN, SWE, SVN, SVK, SMR, SOM, STP, SLV, SEN, TLS, TUN, TUR, UKR, VAT, VGB, USA, SGP, CAN, JPN, IND, AFG, ATG, AIA, XKX, ARM, AGO, ATA, ARG, ASM, AUS, ABW, ALA, BRB, BGD, BFA, BDI, BEN, BLM, BMU, BRN, BOL, BES, BHS, BTN, BVT, BWA, BLZ, CCK, COD, CAF, COG, CIV, COK, CHL, CMR, CHN, COL, CUB, CPV, CUW, CXR, DJI, DMA, DZA, ECU, ESH, ERI, ETH, FJI, FSM, GAB, GRD, GUF, GGY, GHA, GMB, GIN, GLP, GNQ, SGS, GUM, GNB, GUY, HKG, HMD, HND, HTI, IDN, IMN, IOT, IRN, JEY, JAM, KEN, KGZ, KHM, KIR, COM, KNA, PRK, KOR, CYM, LAO, LKA, LBR, LSO, MAR, MAF, MDG, MHL, MLI, MMR, MAC, MNP, MTQ, MSR, MDV, MWI, MEX, MYS, MOZ, NAM, NCL, NER, NFK, NGA, NIC, NPL, NRU, NIU, NZL, PAN, PER, PYF, PNG, PHL, SPM, PCN, PRI, PLW, PRY, REU, RWA, SLB, SHN, SJM, SLE, SUR, SSD, SXM, SYR, SWZ, TCA, TCD, ATF, TGO, THA, TJK, TKL, TKM, TON, TTO, TUV, TWN, TZA, UGA, UMI, URY, UZB, VCT, VEN, VIR, VNM, VUT, WLF, WSM, YEM, MYT, ZAF, ZMB, ZWE
}